(function($){

  $('.single_add_to_cart_button, .add_to_cart_button ').on('click', function(){
      append_html();
      $('.ars-woo-popup-ajax').addClass('animation-dropdown');
      setTimeout(function(){
        destroy_html();
    }, 5000);

  })

  function append_html(){
    var mainDom = document.getElementsByTagName('body');
    var mainDivPopUp = document.createElement( 'div' );
    var nodeParagraph = document.createElement( 'p' );
    var contentText = document.createTextNode( 'Produk anda sudah ditambahan di keranjang');

    document.body.appendChild( mainDivPopUp );
    mainDivPopUp.appendChild( nodeParagraph );
    mainDivPopUp.className += 'ars-woo-popup-ajax';
    nodeParagraph.appendChild( contentText );


  }

  function destroy_html(){
    $('.ars-woo-popup-ajax').remove();
  }


})(jQuery);
