<?php
/**
 * Plugin Name:			Simple popup ajax add to card Woocommerce.
 * Description:			show pop-up after the document.
 * Version:				1.0.0
 * Author:				M. Aris Setiawan
 * Author URI:			https://madebyaris.net
 * Requires at least:	4.5.0
 *
 * Text Domain: kreatif-addon
 */

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}



class simple_Popup_Ajax_Woo {

	Private $prefix = 'simple-wow-popup-ajax';

	public function __construct(){

		// Stop the function if WooCommerce is not active or in the dashboard.
		// Optimize every single line, and learn how to become a better craftman

		if ( false === $this->is_woo_active() ){
			return;
		}

		add_action( 'wp_enqueue_scripts', array( $this, 'register_scripts' ) );
		add_action( 'wp_enqueue_scripts', array( $this, 'register_styles' ) );
	}

	public function register_scripts(){
		wp_register_script( $this->prefix . '-script', plugin_dir_url( __FILE__ ) . 'assets/js/scripts.js', array('jquery'), null, true  );
		wp_enqueue_script( $this->prefix . '-script' );
	}

	public function register_styles(){
		wp_register_style( $this->prefix . '-style', plugin_dir_url( __FILE__ ) . 'assets/css/style.css' );
		wp_enqueue_style( $this->prefix . '-style' );
	}

	private function is_woo_active(){
		include_once( ABSPATH . 'wp-admin/includes/plugin.php' );

		return is_plugin_active( 'woocommerce/woocommerce.php' );
	}

	private function is_dashboard(){
		return is_admin();
	}


}
new simple_Popup_Ajax_Woo();
